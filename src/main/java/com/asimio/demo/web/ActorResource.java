package com.asimio.demo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.asimio.demo.dvdrental.dao.ActorDao;
import com.asimio.demo.dvdrental.model.Actor;

@RestController
@RequestMapping(value = "/actors")
@Transactional
public class ActorResource {

	@Autowired
	private ActorDao actorDao;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getActor(@PathVariable(value = "id") String id) {
		Actor actor = this.actorDao.getOne(Integer.valueOf(id));
		return String.format("[actor: %s %s]", actor.getFirstName(), actor.getLastName());
	}
}