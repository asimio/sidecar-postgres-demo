package com.asimio.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SidecarPostgresDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidecarPostgresDemoApplication.class, args);
	}
}