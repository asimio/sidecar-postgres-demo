package com.asimio.demo.dvdrental.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asimio.demo.dvdrental.model.Actor;

public interface ActorDao extends JpaRepository<Actor, Integer> {
}